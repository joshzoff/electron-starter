import { Component } from '@angular/core';

import './home.component.scss';

@Component({
  selector: 'ee-home',
  template: `
  <div class="home"></div>
  `,
  providers: []
})
export class HomeComponent {};
