/**
 * Import decorators and services from angular
 */
import { Component, OnInit } from '@angular/core';

import '../../style/normalize.scss';
import '../../style/skeleton.scss';
import './app.component.scss';

/*
 * App Component
 * Top Level Component
 */
@Component({
    // The selector is what angular internally uses
    selector: 'ee-app', // <app></app>
    template: `
    <div class='ee-app'>
        <router-outlet></router-outlet>
    </div>
    `
})
export class AppComponent implements OnInit {
    //component initialization
    ngOnInit() {
        //check authentication
    }
}
